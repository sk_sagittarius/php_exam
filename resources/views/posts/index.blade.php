@extends('layouts.app')

@section('content')

    <div class="d-flex align-items-center justify-content-between m-5">
        <h1>{{ $title }}</h1>
        @component('posts._partials.search')@endcomponent
        @if ($isAuthor)
            <div>
                <a href="{{ route('posts.create') }}" class="btn btn-success ml-auto">
                    Create post
                </a>
                <a href="{{ route('categories.create') }}" class="btn btn-success ml-auto">
                    Create category
                </a>
            </div>
        @endif
    </div>

    @forelse($posts as $post)
        <a href="{{ route('posts.show', $post) }}" class="mb-3 m-5 card card-body d-flex flex-column align-items-start">
            <h4 class="mb-0">{{ $post->title }}</h4>
            <div class="d-flex align-items-baseline mt-5">
                <h6>Categories:</h6>
                @foreach($post->categories as $category)
                    <span class="ml-2 bg-light card text-black-50">{{$category->name}}</span>
                @endforeach
            </div>
        </a>
    @empty
        <div class="m-5 alert alert-secondary">
            No posts yet...
        </div>
    @endforelse

@endsection
