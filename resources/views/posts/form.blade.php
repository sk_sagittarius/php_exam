<?php
$update = isset($post);
?>

@extends('layouts.app')

@section('content')
    <h1 class="m-5">{{ $title }}</h1>
    <form action="{{ $update ? route('posts.update', $post) : route('posts.store') }}" method="POST" class="card card-body m-5">
        @csrf
        @if ($update)
            @method('PUT')
        @endif
        <div class="form-group">
            <label for="title">Title <span class="text-danger">*</span></label>
            <input type="text"
                   name="title"
                   id="title"
                   class="form-control @error('title') is-invalid @enderror"
                   placeholder="Enter title..."
                   value="{{ old('title') ?? ($post->title ?? '') }}">
            @error('title')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="content">Post <span class="text-danger">*</span></label>
            <textarea name="content"
                      id="content"
                      class="form-control @error('content') is-invalid @enderror"
                      placeholder="Enter text...">{{ old('content') ?? ($post->content ?? '') }}</textarea>
            @error('content')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="category">Category</label>
            <select name="category[]" id="category" multiple class="custom-select-sm align-bottom w-25 form-control" >
                @foreach($categories as $category)
                    <option class="form-text form-control"
                            value="{{ $category->id }}"
                            @if(isset($postCategories) && in_array($category->id, $postCategories->toArray())) selected @endif>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div>
            <button class="btn btn-success">{{ $update ? 'Update' : 'Add' }}</button>
        </div>

    </form>

@endsection
