<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function create()
    {
        return view('categories.form', [
            'title' => 'New category'
        ]);
    }
    protected function rules() {
        return [
            'searchText' => 'required|min:3'
        ];
    }
    protected function rulesForCategory() {
        return [
            'name' => 'required|min:3'
        ];
    }
    public function search(Request $request)
    {
        $request->validate($this->rules());
        $searchText = $request->searchText;
        $categoryIds = Category::query()->where('name', 'like', "%{$searchText}%")->pluck('id');
        $postsIds = Post::query()
            ->join('category_post', 'id', '=','category_post.post_id')
            ->whereIn('category_post.category_id', $categoryIds)->select('id')->groupBy('id')->pluck('id');
        $posts = Post::query()->whereIn('id', $postsIds)->get();
        return view('categories.index', [
            'title' => 'Search result for ' . $searchText,
            'posts' => $posts
        ]);
    }

    public function store(Request $request)
    {
        $request->validate($this->rulesForCategory());
        $data = $request->except('_token');
        $newCategory = new Category();
        $newCategory->name = $data['name'];
        $newCategory->save();

        return redirect()->route('posts.index');
    }
}
