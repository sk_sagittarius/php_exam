<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\View;
use Illuminate\Http\Request;
use function auth;
use function optional;

class ViewController extends Controller
{
    function count(Post $post) {
        return [
            'count' => $post->views->count
        ];
    }

    function addView(Post $post) {
        if(!$post->views){
            $view = new View();
            $view->post_id = $post->id;
            $view->count = 1;
            $view->save();
        }
        else{
            $count = $post->views->count;
            $count++;
            $post->views->update(['count'=>$count]);
        }
        return [
            'status' => 200
        ];
    }
}
