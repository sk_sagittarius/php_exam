<?php


namespace App\Policies;

use App\Models\Post;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user, Post $post)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->isAuthor == 1;;
    }

    public function update(User $user, Post $post)
    {
        return $user->isAuthor == 1;
    }

    public function delete(User $user, Post $post)
    {
        return $user->isAuthor == 1;
    }
}
