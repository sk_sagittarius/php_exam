<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\User;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\Promise\all;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $user = Auth::user();
        if($user){
            $isAuthor = $user->isAuthor;
        }
        else{
            $isAuthor = 0;
        }
        return view('posts.index', [
            'title' => 'Blog',
            'posts' => Post::all(),
            'isAuthor' => $isAuthor
        ]);
    }

    public function create()
    {
        $categories = Category::all();
        return view('posts.form', [
            'title' => 'New post',
            'categories' => $categories
        ]);
    }

    protected function rules() {
        return [
            'title' => 'required|min:5',
            'content' => 'required|min:10'
        ];
    }

    public function store(Request $request)
    {
        $request->validate($this->rules());

        $data = $request->except('_token');
        $user = auth()->user();
        $post = $user->posts()->create($data);
        if(isset($request->category)){
            foreach ($request->category as $category){
                $post->categories()->attach($category);
            }
        }

        return redirect()->route('posts.show', $post);
    }

    public function show(Post $post)
    {
        $user = Auth::user();
        if($user){
            $isAuthor = $user->isAuthor;
        }
        else{
            $isAuthor = 0;
        }
        return view('posts.show', [
            'title' => $post->title,
            'post' => $post,
            'isAuthor' => $isAuthor
        ]);
    }

    public function edit(Post $post)
    {
        $this->authorize('update', $post);
        $categories = Category::all();
        $postCategories = $post->categories()->pluck('category_id');
        return view('posts.form', [
            'title' => 'Edit ' . $post->title,
            'post' => $post,
            'categories' => $categories,
            'postCategories' => $postCategories
        ]);
    }

    public function update(Request $request, Post $post)
    {
        $this->authorize('update', $post);
        $request->validate($this->rules());

        $data = $request->except(['_token', '_method', 'category']);
        $post->fill($data);
        $post->categories()->detach();
        if(isset($request->category)){
            foreach ($request->category as $category){
                $post->categories()->attach($category);
            }
        }
        $post->save();

        return redirect()->route('posts.show', $post);
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        $post->delete();
        return redirect()->route('posts.index');
    }
}
