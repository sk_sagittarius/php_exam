<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name'
    ];

    function post() {
        return $this->belongsToMany(Post::class);
    }
}
