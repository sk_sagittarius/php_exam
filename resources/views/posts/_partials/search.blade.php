<form action="{{ route('categories.search') }}" method="POST" role="search">
    {{ csrf_field() }}
    <div class="input-group d-flex flex-column form-group">
        <label for="searchText" class="col-form-label ">You can search post by category</label>
        <input type="text"
               class="form-control @error('searchText') is-invalid @enderror form-control-lg align-baseline"
               name="searchText"
               id="searchText"
               placeholder="Enter category name...">
        <button type="submit" class="btn btn-default card justify-content-around text-center btn-outline-dark">
            <span class="text-center text-black-50">Search</span>
        </button>
        @error('searchText')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
    </div>
</form>
