<?php
use Illuminate\Support\Str;
$id = Str::random(10);
?>

<div class="mb-3 m-5 btn-group btn-group-lg">

    @if ($isAuthor)
        <a class="btn btn-info" href="{{ route('posts.edit', $post) }}">
            Edit
        </a>
    @endif

    @if ($isAuthor)
        <a class="btn btn-danger" id="{{ $id }}" href="{{ route('posts.destroy', $post) }}" data-target="delete-{{ $id }}">
            Delete
        </a>
    @endif

</div>

@if ($isAuthor)

    <form id="delete-{{ $id }}" action="{{ route('posts.destroy', $post) }}" method="POST" class="d-none">
        @csrf
        @method('DELETE')
    </form>

    <script>
        let link = document.getElementById('{{ $id }}');
        if (link) {
            let id = link.dataset.target;
            link.addEventListener('click', function (event) {
                event.preventDefault();
                let form = document.getElementById(id);
                form.submit();
            });
        }
    </script>

@endif
