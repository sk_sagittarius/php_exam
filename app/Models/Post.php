<?php

namespace App\Models;

use App\Models\Traits\HasComments;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasComments;

    protected $fillable = [
        'title', 'content', 'user_id'
    ];

    function user() {
        return $this->belongsTo(User::class);
    }
    function views() {
        return $this->hasOne(View::class);
    }
    function categories() {
        return $this->belongsToMany(Category::class);
    }
}
