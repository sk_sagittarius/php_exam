@extends('layouts.app')

@section('content')
    <h1 class="m-5">{{ $title }}</h1>
    <form action="{{ route('categories.store') }}" method="POST" class="card card-body m-5">
        @csrf
        <div class="form-group">
            <label for="name">Name <span class="text-danger">*</span></label>
            <input type="text"
                   name="name"
                   id="name"
                   class="form-control @error('name') is-invalid @enderror"
                   placeholder="Enter title..."
                   value="{{ old('title') }}">
            @error('name')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
            @enderror
        </div>

        <div>
            <button class="btn btn-success">Add</button>
        </div>

    </form>

@endsection
