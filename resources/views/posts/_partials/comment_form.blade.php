<form action="{{ route('comments.store') }}" method="POST" class="mb-3 m-5">

    @csrf

    <input type="hidden" name="post_id" value="{{ $post->id }}">
    <label for="content">Comments</label>

    @error('content')
    <div class="alert alert-danger mb-3">
        Comment can not be empty
    </div>
    @enderror

    <textarea name="content" id="content" class="form-control" placeholder="Your comment..."></textarea>

    <div>
        <button class="btn btn-secondary mt-2 btn-sm">Send</button>
    </div>

</form>
