<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::redirect('/', '/posts');
Route::resource('posts', 'PostController');
Route::resource('categories', 'CategoryController');

Route::post('/search', 'CategoryController@search')
    ->name('categories.search');

Route::name('comments.')
    ->prefix('comments')
    ->middleware('auth')
    ->group(function () {

        Route::post('/', 'CommentController@store')
            ->name('store');
        Route::delete('/{comment}', 'CommentController@destroy')
            ->name('destroy');

    });

Route::post('/posts/{post}/views/count', 'ViewController@count')
    ->name('posts.views.count');
Route::post('/posts/view/{post}', 'ViewController@addView')
    ->name('posts.addView');
