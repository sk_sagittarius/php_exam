<div class="card card-body m-5 mb-auto">

    <h1>{{ $post->title }}</h1>
    <p class="lead">{{ $post->content }}</p>

    {{ $slot }}

</div>
