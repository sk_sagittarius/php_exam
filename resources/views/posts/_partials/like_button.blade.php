<?php
use Illuminate\Support\Str;
$id = Str::random();
?>

<div class="d-flex">
    <span id="view-{{ $id }}" class="ml-auto text-black-50">
        Views: <span class="count"></span>
    </span>
</div>

<script>
    function updateLikesCount(view) {
        axios.post( '{{ route('posts.views.count', $post) }}' ).then(function (res) {
            let count = res.data['count'];
            let span = view.querySelector('span');
            span.innerHTML = count;
        });
    }
    let view = document.getElementById('view-{{ $id }}');
    window.onload = function() {
        axios.post( '{{ route('posts.addView', $post) }}' ).then(function (response) {
            let data = response.data;
            if (data.status === 200) {
                updateLikesCount(view);
            }
        });
    };
</script>
